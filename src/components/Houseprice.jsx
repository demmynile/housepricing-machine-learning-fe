import React from 'react'
import './Houseprice.css'
import logo from '../Assets/estate.png'
import { HiArrowLeft } from "react-icons/hi";
import { useNavigate } from 'react-router-dom';



 const Houseprice = () => {
     
    const navigate = useNavigate()

    const navigateToHome = () => {
        navigate('/')
    }

    return (
       <> 
        
        <div className = "housepriceContainer">
            <div className = "housepriceMain">
               <HiArrowLeft onClick = {navigateToHome} size = {30} style={{color : 'rgb(20, 112, 182)', pointer : 'cursor', }}/>
                <div className ="houseAvatar">
                <img src = {logo} alt = "" className = "houseImage"/>   
                </div>
                <div className ="houseForms">
                    <div className = "houseMainForms">
                        <header>Predict Your House price now...</header><br/><br/>

                        <input type = "number" placeholder = "new price" className = "houseNewPrice1" />
                        <input type = "number" placeholder = "service price" className = "houseNewPrice2" />
                        <input type = "number" placeholder = "estate price" className = "houseNewPrice3" />
                        <input type = "text" placeholder = "location rank" className = "houseNewPrice4" />
                        <input type = "number" placeholder = "toilets" className = "houseNewPrice2" />
                        <input type = "number" placeholder = "bathrooms" className = "houseNewPrice3" />
                        <input type = "text" placeholder = "bedrooms" className = "houseNewPrice4" />
                        <select className = "housePriceSelect">
                            <option> Where is the location of the house</option>
                            <option>gbagada</option>
                            <option>surulere</option>
                            <option>yaba</option>
                            <option>ikorodu</option>
                            <option>ogba</option>
                            <option>iyanaipaja</option>
                            <option>ikeja</option>
                            <option>ikoyi</option>
                            <option>ajah</option>
                            <option>lekki</option>
                           
                        </select>
                        <select className = "housePriceSelect">
                            <option> Do you want an Executive Suit</option>
                            <option>Yes</option>
                            <option>No</option>
                           
                        </select>

                        <select className = "housePriceSelect">
                            <option> Do you want a Terrace</option>
                            <option>Yes</option>
                            <option>No</option>
                           
                        </select>

                        <select className = "housePriceSelect">
                            <option> Do you want a Terrace</option>
                            <option>Yes</option>
                            <option>No</option>
                           
                        </select>
                        <button className = "housePriceButton">Predict</button>

                        <div className = "housePriceReveal">
                        </div>
                        

                      

                    </div>
                </div>

            </div>
        </div>


     </>
    )
}

export default Houseprice